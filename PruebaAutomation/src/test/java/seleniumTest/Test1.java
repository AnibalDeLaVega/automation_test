package seleniumTest;


import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.support.ui.Select;


public class Test1 {
	
	private static WebDriver drv;

	public static void main(String[] args) throws InterruptedException {
		
		/* PARAMETRIA Y ABRIR BROWSER */
		System.setProperty("webdriver.chrome.driver", "C:\\COMAFI\\WorkSpaceTest\\PruebaAutomation\\target\\drivers\\chromedriver.86.0.4240.22.exe");
		drv=new ChromeDriver();
		drv.manage().window().maximize();
		drv.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		drv.navigate().to("http://shop.demoqa.com/");
		//drv.get("http://shop.demoqa.com/");
		
		/* ACCIONES y VERIFICACIONES */
		// Selecciono el boton buscar
		Thread.sleep(3500);
		WebElement btnSearch=drv.findElement(By.cssSelector(".noo-search"));
		System.out.println(btnSearch.getText());
		btnSearch.click();

		// Ingreso un texto a buscar
		WebElement lblSearch=drv.findElement(By.cssSelector(".note-search"));
		System.out.println(lblSearch.getText());
		WebElement tbxSearch=drv.findElement(By.cssSelector(".form-control"));
		tbxSearch.sendKeys("black");
		
		// Presiono Enter para buscar por la palabra ingresada
		tbxSearch.sendKeys(Keys.ENTER);
		Thread.sleep(3500);
		
		// Selecciono la tarjeta deseada
		List<WebElement>lstItems=drv.findElements(By.cssSelector(".noo-product-inner"));
		System.out.println(lstItems.size());
		for (int x=0; x<=lstItems.size()-1; x++) {
			System.out.println("\n"+lstItems.get(x).getText());
		}
		lstItems.get(0).click();Thread.sleep(4000);
		
		// Indico las opciones necesarias para continuar
		
		/*
		 Select nmObject = new Select ( drv.findElement(By.cssSelector(".nombreClass") );
		 nmObject.selectByIndex(1);
		 nmObject.selectByValue("36");
		 nmObject.selectByVisibleText("36");
		*/ 
		
		WebElement optColor1=drv.findElement(By.xpath("//select[@id='pa_color']"));
		optColor1.click();
		WebElement optColor2=drv.findElement(By.xpath("//select[@id='pa_color']/option[2]"));
		System.out.println("\n"+optColor2.getText());optColor2.click();
		WebElement optSize1=drv.findElement(By.xpath("//select[@id='pa_size']"));
		optSize1.click();
		WebElement optSize2=drv.findElement(By.xpath("//select[@id='pa_size']/option[2]"));
		System.out.println("\n"+optSize2.getText());
		optSize2.click();
		WebElement icnMas=drv.findElement(By.cssSelector(".icon_plus"));
		icnMas.click();
		WebElement btnAddCart=drv.findElement(By.cssSelector(".single_add_to_cart_button"));
		System.out.println("\n"+btnAddCart.getText());
		btnAddCart.click();
		
		// Hago clic en el carrito de compra
		WebElement btnCart=drv.findElement(By.cssSelector(".cart-name-and-total"));
		System.out.println("\n"+btnCart.getText());
		btnCart.click();Thread.sleep(4000);
		drv.navigate().to("http://shop.demoqa.com/checkout/");
		
		// Verifico que estoy en el cierre del ticket de compra
		WebElement lblTitle=drv.findElement(By.cssSelector(".page-title"));
		System.out.println(lblTitle.getText());
		Assert.assertEquals("CHECKOUT", lblTitle.getText());
		Assert.assertEquals("\nSe mostro la página CHECKOUT correctamente. ", "CHECKOUT", lblTitle.getText());
		
		/* DESCONEXION Y CERRAR BROWSER */
		Thread.sleep(3500);
		drv.close();
		drv.quit();
		
	}

}
