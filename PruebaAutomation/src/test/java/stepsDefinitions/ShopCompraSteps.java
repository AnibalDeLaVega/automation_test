package stepsDefinitions;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import pageObject.ShopComprasPage;

public class ShopCompraSteps {
	
	private static WebDriver drv;
	ShopComprasPage compra;
	

	@Given("^que abro chrome y navego a la pagina \"([^\"]*)\"$")
	public void que_abro_chrome_y_navego_a_la_pagina(String arg1) throws Throwable {
	    System.out.println("Abre el navegador");
		System.setProperty("webdriver.chrome.driver", "C:\\COMAFI\\WorkSpaceTest\\PruebaAutomation\\target\\drivers\\chromedriver.86.0.4240.22.exe");
		drv=new ChromeDriver();
		drv.manage().window().maximize();
		drv.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		drv.navigate().to("http://shop.demoqa.com/");
	}

	@Given("^que hago clic en buscar$")
	public void que_hago_clic_en_buscar() throws Throwable {
		System.out.println("hago clic en buscar");
		Thread.sleep(3500);
		compra=new ShopComprasPage(drv);
		compra.clicBuscar();
	}

	@Given("^que ingreso el texto \"([^\"]*)\" y presiono enter para buscar$")
	public void que_ingreso_el_texto_y_presiono_enter_para_buscar(String textoBuscar) throws Throwable {
		System.out.println("ingreso el texto "+textoBuscar+" y presiono enter para buscar");
		compra=new ShopComprasPage(drv);
		compra.ingresaTextoBuscar(textoBuscar);
		compra.enterTextoBuscar();
		Thread.sleep(3500);
	}

	@When("^selecciono la tarjeta nro (\\d+) de la lista$")
	public void selecciono_la_tarjeta_nro_de_la_lista(int indice) throws Throwable {
		System.out.println("selecciono la tarjeta nro "+indice+" de la lista");
		compra=new ShopComprasPage(drv);
		compra.recorreLista();
		compra.seleccionItem(indice);
		Thread.sleep(4000);
	}

	@When("^ingreso la opción (\\d+) como color y la opcion (\\d+) como talle$")
	public void ingreso_la_opción_como_color_y_la_opcion_como_talle(int color, int talle) throws Throwable {
		System.out.println("ingreso la opción "+color+" como color y la opcion "+talle+" como talle");
		compra=new ShopComprasPage(drv);
		compra.selectColor();
		compra.selectColorOpcion();
		compra.selectTalle();
		compra.selectTalleOpcion();
		compra.seleccionaSegundaImagen();
		compra.clicAgregarAlCarro();
	}

	@When("^hago clic en el carrito de compra$")
	public void hago_clic_en_el_carrito_de_compra() throws Throwable {
		System.out.println("hago clic en el carrito de compra");
		compra=new ShopComprasPage(drv);
		compra.clicCarrito();
	}

	@When("^hago clic en proceder a la compra$")
	public void hago_clic_en_proceder_a_la_compra() throws Throwable {
		System.out.println("hago clic en proceder a la compra");
		Thread.sleep(4000);
		drv.navigate().to("http://shop.demoqa.com/checkout/");
	}

	@Then("^se muestra \"([^\"]*)\" en la página$")
	public void se_muestra_en_la_página(String opcion) throws Throwable {
		System.out.println("se muestra "+opcion+" en la página");
		compra=new ShopComprasPage(drv);
		Assert.assertEquals(opcion, compra.textLblChecout());
		Assert.assertEquals("\nSe mostro la página "+opcion+" correctamente. ", opcion, compra.textLblChecout());
		
		Thread.sleep(3500);
		drv.close();
		drv.quit();
	}

	
}
