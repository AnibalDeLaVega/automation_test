package runnersClass;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features="src/resources/features_test",
		glue= {"stepsDefinitions"}
		)

public class runnerTest {

}
