Feature: Prueba sitio de compras

	Scenario: Comprar un producto en forma exitosa
		Given que abro chrome y navego a la pagina "http://shop.demoqa.com/"
		And que hago clic en buscar
		And que ingreso el texto "white" y presiono enter para buscar
		When selecciono la tarjeta nro 1 de la lista
		And ingreso la opción 1 como color y la opcion 1 como talle
		And hago clic en el carrito de compra
		And hago clic en proceder a la compra
		Then se muestra "CHECKOUT" en la página
		