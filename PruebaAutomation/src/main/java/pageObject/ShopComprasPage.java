package pageObject;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class ShopComprasPage {
	
	public ShopComprasPage(WebDriver drv) {
		PageFactory.initElements(drv, this);
	}
	
	// LOCALIZADORES CON FindBy
	@FindBy(how=How.CSS,using=".noo-search")
	private WebElement btnBuscar;
	@FindBy(how=How.CSS,using=".form-control")
	private WebElement tbxBuscar;
	@FindBy(how=How.CSS,using=".noo-product-inner")
	private List<WebElement> listItems;
	@FindBy(how=How.XPATH,using="//select[@id='pa_color']")
	private WebElement optColor;
	@FindBy(how=How.XPATH,using="//select[@id='pa_color']/option[2]")
	private WebElement optColorOpcion;
	@FindBy(how=How.XPATH,using="//select[@id='pa_size']")
	private WebElement optTalle;
	@FindBy(how=How.XPATH,using="//select[@id='pa_size']/option[2]")
	private WebElement optTalleOpcion;
	@FindBy(how=How.XPATH,using="//div[@class='noo-woo-thumbnails']/div[2]")
	private WebElement imgTwo;
	@FindBy(how=How.CSS,using=".single_add_to_cart_button")
	private WebElement btnAddCort;
	@FindBy(how=How.CSS,using=".cart-name-and-total")
	private WebElement btnCarrito;
	@FindBy(how=How.CSS,using=".icon_plu")
	private WebElement icnMas;
	@FindBy(how=How.CSS,using=".page-title")
	private WebElement lblCheckout;
	
	//METODOS Y ACCIONES
	public String textoBuscar() {
		return (String) btnBuscar.getText();
	}
	public void clicBuscar() {
		btnBuscar.click();
	}
	public void ingresaTextoBuscar(String textoBuscar) {
		tbxBuscar.sendKeys(textoBuscar);
	}
	public void enterTextoBuscar() {
		tbxBuscar.sendKeys(Keys.ENTER);
	}
	public void recorreLista() {
		for (int x=0; x<=listItems.size()-1; x++) {
			System.out.println("\n"+listItems.get(x).getText());
		}
	}
	public void seleccionItem(int indice) {
		listItems.get(indice).click();
	}
	public void selectColor() {
		optColor.click();
	}
	public void selectColorOpcion() {
		optColorOpcion.click();
	}
	public void selectTalle() {
		optTalle.click();
	}
	public void selectTalleOpcion() {
		optTalleOpcion.click();
	}
	public void seleccionaSegundaImagen() {
		imgTwo.click();
	}
	public void clicSumarUno() {
		icnMas.click();
	}
	public void clicAgregarAlCarro() {
		btnAddCort.click();
	}
	public void clicCarrito() {
		btnCarrito.click();
	}
	public String textLblChecout() {
		return lblCheckout.getText();
	}
	
}
